class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        
        int rows = matrix.length;
        int cols = matrix[0].length;
        
        // Start from the top-right corner
        int row = 0;
        int col = cols - 1;
        
        // Iterate until we're within the bounds of the matrix
        while (row < rows && col >= 0) {
            if (matrix[row][col] == target) {
                return true; // Found the target
            } else if (matrix[row][col] < target) {
                // If current element is less than target, move to the next row
                row++;
            } else {
                // If current element is greater than target, move to the previous column
                col--;
            }
        }
        
        // Target not found in the matrix
        return false;
    }
}

public class Main {
    public static void main(String[] args) {
        int[][] matrix = {{3, 30, 38}, {44, 52, 54}, {57, 60, 69}};
        int target = 62;
        
        Solution solution = new Solution();
        boolean result = solution.searchMatrix(matrix, target);
        System.out.println(result ? 1 : 0);
    }
}
